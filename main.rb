# frozen_string_literal: true

require_relative 'lib/log_parser'

file_path = 'games.log'

parser = LogParser.new(file_path)

puts parser.first_line

puts parser.to_json
