# Desafio Log Parser
**This task requires to create a parser for "games.log" file.**

## Getting Started

This project involves:

1. Installing Ruby
2. Installing Bundler Package Manager
3. Clone repository
4. Installing dependencies
5. Run the tests and application

### 1. Installing Ruby

There are two ways for installing ruby:

- Through the [RVM](https://rvm.io/rvm/install)
- Through the [ASDF](http://asdf-vm.com/guide/getting-started.html#_1-install-dependencies)

Choose what suits you best!

Just be sure to install the 3.1.0 version. You can check the version of ruby with this command:

```ruby -v```

### 2. Installing bundle

After installing ruby, get bundler:

```gem install bundler```

### 3. Clone repository

Clone this repository:

```git clone https://gitlab.com/heitor.cardozo1/desafio-log-parser```

### 4. Installing dependencies

Open your terminal in the root folder and run:

```bundle install```

After that, you`re ready to run the applicaton and the tests!

### 5. Run the tests and application

First, it's necessary to add the file "games.log" to the root folder. You can download the file through this [link.](https://gist.github.com/fabiosammy/ba973184e82e930043df8d4aa002bde4)

To run the application file, execute the following command on the root folder:

```ruby main.rb```

To run the test file, execute the following command on the root folder:

```rspec```
