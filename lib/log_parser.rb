# frozen_string_literal: true

require 'json'

# A parser for the file "games.log"
class LogParser
  def initialize(path)
    @path = path
    raise 'File is missing' unless File.file?(@path)

    @file_content = open_file(@path)
  end

  def first_line
    @file_content.first
  end

  def to_json
    obj = {
      @path => {
        'lines' => @file_content.length
      }
    }
    stringify_json(obj)
  end

  private

  def open_file(value)
    File.readlines(value, chomp: true)
  end

  def stringify_json(hash)
    JSON.pretty_generate(hash)
  end
end
