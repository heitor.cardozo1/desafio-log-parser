# frozen_string_literal: true

require_relative '../spec_helper'
require_relative '../../lib/log_parser'

RSpec.describe LogParser do
  it 'Checks if the does not file exists' do
    expect { LogParser.new('spec/fixtures/games_test2.log') }.to raise_error(RuntimeError, 'File is missing')
  end

  describe '#first_line' do
    it 'Checks the first line of the file' do
      parser = LogParser.new('spec/fixtures/games_test.log')
      expect(parser.first_line).to eq('  0:00 ------------------------------------------------------------')
    end
  end

  describe '#line_counter' do
    it 'Counts the number of lines' do
      obj = {
        'spec/fixtures/games_test.log' => {
          'lines' => 10
        }
      }
      parser = LogParser.new('spec/fixtures/games_test.log')
      expect(parser.to_json).to eq(JSON.pretty_generate(obj))
    end
  end
end
